import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/utils/not-found/not-found.component';
import { HomeComponent } from './components/utils/home/home.component';
import { SigninComponent } from './components/auth/signin/signin.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { SettingsComponent } from './components/client/settings/settings.component';
import { AuthGuard } from './guards/auth.guard';
import { MessengerComponent } from './components/client/messenger/messenger.component';


const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: "full" },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'settings', canActivate: [AuthGuard], component: SettingsComponent },
  { path: 'message', canActivate: [AuthGuard], component: MessengerComponent },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
