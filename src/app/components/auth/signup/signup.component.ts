import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'csdt-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  email = '';
  pwd = '';
  name = '';
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.verifyUser();
    this.authService.observeAuthUser.subscribe(res => {
      if (res) { this.router.navigate(['settings']); }
    });
  }

  signUp(): void {
    const user = {
      email: this.email.trim(),
      pwd: this.pwd.trim(),
      name: this.name.trim()
    };
    if (!user.email || !user.pwd || !user.name) { return; }
    this.authService.signUp(user, (err, res) => {
      if (!res['error']) {
        this.router.navigate(['signin']);
      }
    });
  }

}
