import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'csdt-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  user = {
    username: "",
    password: ""
  };

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.verifyUser();
    this.authService.observeAuthUser.subscribe(res => {
      if (res) { this.router.navigate(['settings']); }
    });
  }

  signIn(): void {
    this.user.username = this.user.username.trim();
    this.user.password = this.user.password.trim();
    if (!this.user.username || !this.user.password) { return; }
    this.authService.signIn(this.user);
  }
}
