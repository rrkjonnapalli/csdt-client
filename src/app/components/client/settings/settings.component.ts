import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TwitService } from 'src/app/services/twit.service';
import { DataService } from 'src/app/services/data.service';
import { AuthService } from 'src/app/services/auth.service';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'csdt-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  twitUrl = `${environment.apiHost}/twitter`;
  user: any;
  signOutIcon = faSignOutAlt;
  constructor(private dataService: DataService, private authService: AuthService, private twitService: TwitService) {
    this.user = {};
  }

  ngOnInit() {
    this.dataService.observeUser.subscribe(res => {
      if (res) {
        this.user = res;
      }
    });
  }

  connectWithTwitter(): void {
    this.twitService.connectWithTwitter();
  }

  signOut(): void {
    this.authService.signOut();
  }

}
