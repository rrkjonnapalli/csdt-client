import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { TwitService } from 'src/app/services/twit.service';
import { SocketService } from 'src/app/services/socket.service';
import { Observable } from 'rxjs';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'csdt-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.scss']
})
export class MessengerComponent implements OnInit {
  authUser: Object;
  twitUsers: Observable<Array<Object>>;
  signOutIcon = faSignOutAlt;
  messages: Array<Object>;
  selectedUser: any;
  msg: String;

  constructor(private authService: AuthService, private dataService: DataService, private twitSerivce: TwitService, private router: Router, private socketService: SocketService) {
    this.twitUsers = this.twitSerivce.observeTwitUsers;
    this.selectedUser = {};
  }

  ngOnInit() {
    this.dataService.observeUser.subscribe(res => {
      if (res) {
        this.authUser = res;
        if (res['role'] !== 'admin') {
          this.router.navigate(['settings']);
          return;
        }
        this.twitSerivce.getUsers();
      }
    });


    this.socketService.socket.on('tweet', (data) => {
      // if(this.selectedUser)
      if (data.uId === this.selectedUser['_id'] || data.touId === this.selectedUser['_id']) {
        this.messages.push(data);
      }
    });

    this.socketService.socket.on('msg', (data) => {
      console.log(data);
    });

    this.socketService.socket.on('tuser', (data) => {
      this.twitSerivce.getUsers();
    });
  }

  getMessages(user: Object): void {
    this.selectedUser = user;
    this.messages = [];
    this.twitSerivce.getMessages(this.selectedUser['_id'], (err, data) => {
      this.messages = data;
    });
  }

  reply(): void {
    if (!this.msg) { return; }
    this.socketService.socket.emit('replyTweet', { text: this.msg.trim(), id: this.messages.slice(-1)[0]['_id'], uId: this.selectedUser['_id'] });
    this.msg = '';
  }

  signOut(): void {
    this.authService.signOut();
  }

}
