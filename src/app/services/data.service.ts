import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private root = `${environment.apiHost}/user`;
  private user: Object;
  private userSource = new BehaviorSubject<Object>(this.user);
  observeUser = this.userSource.asObservable();

  constructor(private http: HttpClient, private authService: AuthService) {
    this.authService.observeAuthUser.subscribe(res => {
      if (res && res['_id']) {
        this.getUser();
      } else {
        this.user = undefined;
        this.userSource.next(this.user);
      }
    });
  }

  getUser(cb?: Function): void {
    this.user = undefined;
    this.userSource.next(this.user);
    const uri = `${this.root}`;
    this.http.get(uri).subscribe(res => {
      if (!res['error'] && (!this.user || res['data']['_id'] !== this.user['_id'])) {
        this.user = res['data'];
        this.userSource.next(this.user);
      }
      if (cb) { cb(null, res); }
    });
  }
}
