import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private root = `${environment.apiHost}/auth`;

  private authUser: Object;
  private authUserSource = new BehaviorSubject<Object>(this.authUser);
  observeAuthUser = this.authUserSource.asObservable();

  private verified = false;
  private loggedIn = false;

  constructor(private http: HttpClient, private router: Router) {
    // if (localStorage.getItem('loggedIn') === '1') { this.verifyUser(); }
  }

  verifyUser(): Promise<boolean> {
    const uri = `${this.root}/check`;
    return new Promise((resolve, reject) => {
      if (this.verified) { return resolve(this.loggedIn); }
      if (localStorage.getItem('loggedIn') !== '1') { return resolve(false); }
      this.http.get(uri).subscribe(res => {
        this.verified = true;
        this.loggedIn = true;
        if (res['error']) {
          this.loggedIn = false;
          localStorage.removeItem('loggedIn');
          return resolve(false);
        }
        if (!this.authUser || this.authUser['_id'] !== res['data']['_id']) {
          this.authUser = res['data'];
          this.authUserSource.next(this.authUser);
        }
        return resolve(true);
      }, err => {
        return resolve(false);
      });
    });
  }

  signIn(user: Object, cb?: Function): void {
    this.authUser = undefined;
    this.authUserSource.next(this.authUser);
    const uri = `${this.root}/signin`;
    this.http.post(uri, user).subscribe(res => {
      this.verified = true;
      this.loggedIn = false;
      if (!res['error']) {
        this.loggedIn = true;
        localStorage.setItem('loggedIn', '' + 1);
        this.authUser = res['data'];
        this.authUserSource.next(this.authUser);
      }
      if (cb) { cb(null, res); }
    }, err => {
      if (cb) { cb(err); }
    });
  }

  signUp(user: Object, cb?: Function): void {
    const uri = `${this.root}/signup`;
    this.http.post(uri, user).subscribe(res => {
      console.log(res);
      if (cb) { cb(null, res); }
    }, err => {
      if (cb) { cb(err); }
    });
  }

  signOut(cb?: Function): void {
    const uri = `${this.root}/signout`;
    this.http.get(uri).subscribe(res => {
      console.log(res);
      this.loggedIn = false;
      localStorage.removeItem('loggedIn');
      this.authUser = undefined;
      this.authUserSource.next(this.authUser);
      this.router.navigate(['signin']);
      // location.reload();
    });
  }
}
