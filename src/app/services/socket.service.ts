import { Injectable } from '@angular/core';
import io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private root = `${environment.host}`;
  socket: any;
  constructor(private authService: AuthService) {
    this.authService.observeAuthUser.subscribe(res => {
      if (res && res['role'] === 'admin') {
        this.socket = io(this.root);
      }
    });
  }
}
