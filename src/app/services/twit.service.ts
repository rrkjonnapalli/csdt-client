import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TwitService {
  private root = `${environment.apiHost}/twitter`;
  constructor(private http: HttpClient) { }

  private twitUsers = [];
  private twitUsersSource = new BehaviorSubject<Array<Object>>(this.twitUsers);
  observeTwitUsers = this.twitUsersSource.asObservable();

  connectWithTwitter(cb?: Function): void {
    const uri = `${this.root}`;
    this.http.get(uri).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  getUsers(cb?: Function): void {
    this.twitUsers = [];
    this.twitUsersSource.next(this.twitUsers);
    const uri = `${this.root}/users`;
    this.http.get(uri).subscribe(res => {
      console.log(res);
      if (!res['err']) {
        this.twitUsers = res['data'];
        this.twitUsersSource.next(this.twitUsers);
      }
      if (cb) { cb(null, res); }
    }, err => {
      if (cb) { cb(err); }
    });
  }

  getMessages(uid, cb: Function): void {
    const uri = `${this.root}/mentions`;
    this.http.get(uri, { params: { uid: uid.toString() } }).subscribe(res => {
      console.log(res);
      if (!res['error']) {
        cb(null, res['data']);
      } else {
        cb(null, []);
      }
    }, err => {
      console.log(err);
    });
  }
}
