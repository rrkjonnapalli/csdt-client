import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { SigninComponent } from './components/auth/signin/signin.component';
import { NotFoundComponent } from './components/utils/not-found/not-found.component';
import { SettingsComponent } from './components/client/settings/settings.component';
import { HomeComponent } from './components/utils/home/home.component';
import { MessengerComponent } from './components/client/messenger/messenger.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { TwitService } from './services/twit.service';
import { DataService } from './services/data.service';
import { SocketService } from './services/socket.service';
@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    NotFoundComponent,
    SettingsComponent,
    HomeComponent,
    MessengerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FontAwesomeModule
  ],
  providers: [AuthService, AuthGuard, TwitService, DataService, SocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
