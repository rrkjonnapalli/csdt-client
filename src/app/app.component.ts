import { Component } from '@angular/core';

@Component({
  selector: 'csdt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'csdt-client';
}
