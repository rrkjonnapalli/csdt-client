export const environment = {
  production: true,
  apiHost: `http://rrkz.herokuapp.com/api`,
  host: `http://rrkz.herokuapp.com`
};
